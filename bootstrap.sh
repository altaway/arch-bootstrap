#!/usr/bin/env zsh

print "Start\n"

print "Enabling network time sync in the live image"
timedatectl set-ntp true

read -q "Writing partition table"
(
echo o # Create a new empty DOS partition table
echo n # Add a new partition
echo p # Primary partition
echo 1 # Partition number
echo   # First sector (Accept default: 1)
echo   # Last sector (Accept default: varies)
echo w # Write changes
) | fdisk /dev/vda
# cfdisk /dev/vda

print "Creating ext4 filesystem"
mkfs.ext4 /dev/vda1

print "Mounting"
mount /dev/vda1 /mnt

print "Running pacstrap"
pacstrap /mnt base linux linux-firmware \
    networkmanager \ # systemd-networkd
    dnsmasq \ # systemd-resolved
    ntp \ # systemd-timesyncd
    neovim emacs \
    cockpit \
    zsh grml-zsh-config starship \
    git \
    chezmoi \
    arch-install-scripts \
    caddy \
    grub mkinitcpio

print "Generating filesystem table"
genfstab -U /mnt >> /mnt/etc/fstab

print "Mounting underlying partition"
arch-chroot /mnt << EOF

print "Setting timezone to UTC"
ln -sf /usr/share/zoneinfo/UTC /etc/localtime

print "Syncing hardware clock with system clock"
hwclock --systohc

print "Generating and setting locale"
sed 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' -i /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

print "Setting up root user"
chsh -s /bin/zsh
passwd
pushd /root
git clone "https://gitlab.com/altaway/dotfiles.git" .dotfiles
# pushd .dotfiles
# stow --no-folding -R .
chezmoi apply --source=.dotfiles --destination=.

print "Setting up regular user"
useradd -m -G wheel -s /usr/bin/zsh user
passwd user
EOF

chroot /mnt/home/user/ /bin/zsh -u user << EOF 
git clone "https://gitlab.com/altaway/dotfiles.git" .dotfiles
chezmoi apply --source=.dotfiles --destination=/home/user
git clone "https://gitlab.com/altaway/slash-etc.git" .slash-etc
chezmoi apply --source=.slash-etc --destination=/etc
EOF

chroot /mnt << EOF
print "Replacing systemd components"
systemctl disable systemd-networkd systemd-resolved systemd-timesyncd
systemctl enable NetworkManager dnsmasq ntpd

print "Setting network config files"
echo "altaway" > /etc/hostname
echo "nameserver 1.1.1.1\nnameserver 8.8.8.8" > /etc/resolv.conf
echo "127.0.0.1\tlocalhost" > /etc/hosts

print "Installing gtub"
grub-install --target=i386-pc /dev/vda
sed 's/^GRUB_TIMEOUT=5$/GRUB_TIMEOUT=0/' -i /etc/default/grub # zero grub timeout
sed 's/^GRUB_CMDLINE_LINUX_DEFAULT="quiet"$/GRUB_CMDLINE_LINUX_DEFAULT=""/' -i /etc/default/grub # startup and shutdown messages
grub-mkconfig -o /boot/grub/grub.cfg

print "Generating initial ramdisk"
mkinitcpio -P
EOF

print "Finished"
